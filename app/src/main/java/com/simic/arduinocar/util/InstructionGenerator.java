package com.simic.arduinocar.util;

import com.simic.arduinocar.model.Direction;
import com.simic.arduinocar.model.Equipment;
import com.simic.arduinocar.model.Instruction;


/**
 *
 * Created by Nemanja Simic on 29 Jan 2018.
 */

public class InstructionGenerator {

    private Instruction instruction;

    public InstructionGenerator() {
        instruction = new Instruction();
    }

    public Instruction getInstruction() {
        return instruction;
    }

    public void determineDirection(int value) {
        if(Math.abs(value) > 101 && Math.abs(value) < 120)
            return;

        if(value > 100)
            forward(value);
        else
            backward(Math.abs(value));
    }

    public void determineSteering(int value) {
        if(Math.abs(value) > 101 && Math.abs(value) < 120)
            return;

        if(value > 100)
            right(value);
        else
            left(Math.abs(value));
    }

    public void gyroSteer(int value) {
        if(value > -10 && value < 10) {
            right(100);
            left(100);
            return;
        }

        if(value > 40)
            value = 40;

        if(value < -40)
            value = -40;


        if(value > 5) {
            // left
            float rate = (value / 40f) * 30 + 100;
//            Log.d("LEFT", "val " + rate);
            left((int)rate);
        } else if(value < -5) {
            // right
            float rate = (value / -40f) * 30 + 100;
            right((int)rate);
//            Log.d("RIGHT", "val " + rate);
        }
    }

    public void gyroAccel(int value) {
        if(value > 30) {
            reset();
            return;
        }

        if(value < -60 && value > -80) {
            forward(100);
            backward(100);
            return;
        }

        if(value < -110)
            value = -110;

        if(value > -30)
        value = -30;

        int transformedVal = value + 70;

        if(transformedVal > 0) {
            // forward
            float rate = (transformedVal / 40f) * 80 + 100;
            forward((int)rate);
//            Log.d("FWD", "val " + rate);
        } else if(transformedVal < 0) {
            // backward
            float rate = (transformedVal / -40f) * 80 + 100;
            backward((int)rate);
//            Log.d("BCK", "val " + rate);
        }
    }

    private void forward(int value) {
        instruction.setAccelerationRate(value);

        if(value == 100) {
            instruction.setDirection(instruction.getDirection() & Direction.NOT_FORWARD & Direction.NOT_BACKWARD);
            return;
        }
        if((instruction.getDirection() & Direction.BACKWARD ) != Direction.NONE) {
            instruction.setDirection((instruction.getDirection() | Direction.FORWARD) & Direction.NOT_BACKWARD);
        } else {
            instruction.setDirection(instruction.getDirection() | Direction.FORWARD);
        }
    }

    private void backward(int value) {
        instruction.setAccelerationRate(value);

        if(value == 100) {
            instruction.setDirection(instruction.getDirection() & Direction.NOT_BACKWARD & Direction.NOT_FORWARD);
            return;
        }
        if((instruction.getDirection() & Direction.FORWARD ) != Direction.NONE) {
            instruction.setDirection((instruction.getDirection() | Direction.BACKWARD) & Direction.NOT_FORWARD);
        } else {
            instruction.setDirection(instruction.getDirection() | Direction.BACKWARD);
        }
    }

    private void right(int value) {
        instruction.setSteeringRate(value);

        if(value == 100) {
            instruction.setDirection(instruction.getDirection() & Direction.NOT_LEFT & Direction.NOT_RIGHT);
            return;
        }

        if((instruction.getDirection() & Direction.LEFT) != Direction.NONE) {
            instruction.setDirection((instruction.getDirection() | Direction.RIGHT) & Direction.NOT_LEFT);
        } else {
            instruction.setDirection(instruction.getDirection() | Direction.RIGHT);
        }
    }

    private void left(int value) {
        instruction.setSteeringRate(value);

        if(value == 100) {
            instruction.setDirection(instruction.getDirection() & Direction.NOT_LEFT & Direction.NOT_RIGHT);
            return;
        }

        if((instruction.getDirection() & Direction.RIGHT) != Direction.NONE) {
            instruction.setDirection((instruction.getDirection() | Direction.LEFT) & Direction.NOT_RIGHT);
        } else {
            instruction.setDirection(instruction.getDirection() | Direction.LEFT);
        }
    }

    public void startStop() {
        if((instruction.getEquipment() & Equipment.TURN_ON) == Equipment.NONE) {
            instruction.setEquipment(instruction.getEquipment() | Equipment.TURN_ON);
        } else {
            instruction.setEquipment(instruction.getEquipment() & Equipment.TURN_OFF);
        }
    }

    public void lights() {
        if((instruction.getEquipment() & Equipment.HEADLIGHTS) == Equipment.NONE) {
            instruction.setEquipment(instruction.getEquipment() | Equipment.HEADLIGHTS);
        } else {
            instruction.setEquipment(instruction.getEquipment() & Equipment.HEADLIGHTS_OFF);
        }
    }

    public void blink() {
        instruction.setEquipment(instruction.getEquipment() | Equipment.BLINK);
    }

    public void blinkReleased() {
        instruction.setEquipment(instruction.getEquipment() & Equipment.NOT_BLINK);
    }

    public void horn() {
        instruction.setEquipment(instruction.getEquipment() | Equipment.HORN);
    }

    public void hornReleased() {
        instruction.setEquipment(instruction.getEquipment() & Equipment.HORN_OFF);
    }

    public void alarm() {
        if((instruction.getEquipment() & Equipment.ALARM) == Equipment.NONE) {
            instruction.setEquipment(instruction.getEquipment() | Equipment.ALARM);
        } else {
            instruction.setEquipment(instruction.getEquipment() & Equipment.ALARM_OFF);
        }
    }

    public void hazard() {
        if((instruction.getEquipment() & Equipment.HAZARD_LIGHTS) == Equipment.NONE) {
            instruction.setEquipment(instruction.getEquipment() | Equipment.HAZARD_LIGHTS);
        } else {
            instruction.setEquipment(instruction.getEquipment() & Equipment.HAZARD_LIGHTS_OFF);
        }
    }

    public void turnLeft() {
        if((instruction.getEquipment() & Equipment.RIGHT_INDICATOR) == Equipment.RIGHT_INDICATOR)
            instruction.setEquipment(instruction.getEquipment() & Equipment.RIGHT_INDICATOR_OFF);
        if((instruction.getEquipment() & Equipment.HAZARD_LIGHTS) == Equipment.HAZARD_LIGHTS)
            instruction.setEquipment(instruction.getEquipment() & Equipment.HAZARD_LIGHTS_OFF);


        if((instruction.getEquipment() & Equipment.LEFT_INDICATOR) == Equipment.NONE) {
            instruction.setEquipment(instruction.getEquipment() | Equipment.LEFT_INDICATOR);
        } else {
            instruction.setEquipment(instruction.getEquipment() & Equipment.LEFT_INDICATOR_OFF);
        }
    }

    public void turnRight() {
        if((instruction.getEquipment() & Equipment.LEFT_INDICATOR) == Equipment.LEFT_INDICATOR)
            instruction.setEquipment(instruction.getEquipment() & Equipment.LEFT_INDICATOR_OFF);
        if((instruction.getEquipment() & Equipment.HAZARD_LIGHTS) == Equipment.HAZARD_LIGHTS)
            instruction.setEquipment(instruction.getEquipment() & Equipment.HAZARD_LIGHTS_OFF);

        if((instruction.getEquipment() & Equipment.RIGHT_INDICATOR) == Equipment.NONE) {
            instruction.setEquipment(instruction.getEquipment() | Equipment.RIGHT_INDICATOR);
        } else {
            instruction.setEquipment(instruction.getEquipment() & Equipment.RIGHT_INDICATOR_OFF);
        }
    }

    public void reset() {
        instruction.setDirection(Direction.NONE);
        instruction.setAccelerationRate(100);
        instruction.setSteeringRate(100);
    }
}
