package com.simic.arduinocar.connectivity;

import android.util.Log;

import com.simic.arduinocar.model.Instruction;

/**
 *
 * Created by Nemanja Simic on 29 Jan 2018.
 */

public class Router {

    private static Router instance;

    private WiFiConnector connector;

    private Router() {
        connector = new WiFiConnector("http://192.168.7.3/car");
    }

    public static Router getInstance() {
        if(instance == null)
            instance = new Router();

        return instance;
    }

    public void route(Instruction instruction) {
        Log.i("ROUTING", instruction.getCode());
        connector.send(instruction);
    }
}
