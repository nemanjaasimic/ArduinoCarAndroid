package com.simic.arduinocar.connectivity;

import android.util.Log;

import com.simic.arduinocar.model.Instruction;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 *
 * Created by Nemanja Simic on 29 Jan 2018.
 */

public class WiFiConnector {

    private OkHttpClient client;

    private String url;

    public WiFiConnector(String url) {
        client = new OkHttpClient();
        this.url = url;
    }

    public void send(Instruction instruction) {
        if (client != null) {
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
            RequestBody body = RequestBody.create(mediaType, parametrizeInstruction(instruction));
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .addHeader("content-type", "application/x-www-form-urlencoded")
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    Log.e("REQUEST FAILED!", "WiFiConnector");
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d("STATUS CODE", Integer.toString(response.code()));
                    response.close();
                }
            });
        }
    }


    private String parametrizeInstruction(Instruction i) {
        final StringBuilder builder = new StringBuilder();
        builder
                .append("direction=")
                .append(i.getDirection())
                .append("&speed=")
                .append(i.getAccelerationRate())
                .append("&steering=")
                .append(i.getSteeringRate())
                .append("&equipment=")
                .append(i.getEquipment());

        return builder.toString();
    }
}
