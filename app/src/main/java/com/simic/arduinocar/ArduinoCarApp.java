package com.simic.arduinocar;

import android.app.Application;
import android.content.Context;

/**
 *
 * Created by Nemanja Simic on 28 Jan 2018.
 */

public class ArduinoCarApp extends Application {

    private static ArduinoCarApp arduinoCarApp;

    public static Context getAppContext() {
        return arduinoCarApp.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        arduinoCarApp = this;
    }
}
