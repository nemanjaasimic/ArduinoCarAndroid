package com.simic.arduinocar.activity;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.simic.arduinocar.R;
import com.simic.arduinocar.connectivity.Router;
import com.simic.arduinocar.model.Direction;
import com.simic.arduinocar.model.Equipment;
import com.simic.arduinocar.model.Instruction;
import com.simic.arduinocar.util.InstructionGenerator;
import com.simic.arduinocar.view.AccelerationJoystickView;
import com.simic.arduinocar.view.SteeringJoystickView;

public class MainActivity extends AppCompatActivity implements AccelerationJoystickView.AccelerationJoystickListener,
                                                                SteeringJoystickView.SteeringJoystickListener, SensorEventListener {

    private InstructionGenerator generator;

    private boolean connected;

    private boolean useGyro = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        generator = new InstructionGenerator();
        connected = false;
        initButtons();
        initGyro();
    }

    private void initButtons() {
        final Button start = findViewById(R.id.start_button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                generator.startStop();
            }
        });

        final Button lights = findViewById(R.id.lights_button);
        lights.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generator.lights();
            }
        });

        final Button blink = findViewById(R.id.blink_button);
        blink.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    generator.blink();
                } else if(event.getAction() == MotionEvent.ACTION_UP) {
                    generator.blinkReleased();
                }
                return true;
            }
        });

        final Button horn = findViewById(R.id.horn_button);
        horn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN) {
                    generator.horn();
                } else if(event.getAction() == MotionEvent.ACTION_UP) {
                    generator.hornReleased();
                }
                return true;
            }
        });

        final Button alarm = findViewById(R.id.alarm_button);
        alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generator.alarm();
            }
        });

        final Button hazard = findViewById(R.id.hazard_lights_button);
        hazard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generator.hazard();
            }
        });

        final Button turnRight = findViewById(R.id.turn_right_button);
        turnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generator.turnRight();
            }
        });

        final Button turnLeft = findViewById(R.id.turn_left_button);
        turnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generator.turnLeft();
            }
        });

        final Button c = findViewById(R.id.connect_button);
        c.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!connected) {
                    connect();
                } else {
                    disconnect();
                }
            }
        });

        final Switch controlType = findViewById(R.id.control_type_button);
        controlType.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    useGyro = true;
                } else {
                    useGyro = false;
                    generator.reset();
                }
            }
        });
    }

    private void initGyro() {
        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Sensor rotationSensor;
        if (sensorManager != null) {
            rotationSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
            sensorManager.registerListener(this, rotationSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }
    }


    private void connect() {
        connected = true;
        sendInstruction();
    }

    private void disconnect() {
        generator.getInstruction().setDirection(Direction.NONE);
        generator.getInstruction().setEquipment(Equipment.NONE);
        generator.getInstruction().setAccelerationRate(180);
        generator.getInstruction().setSteeringRate(160);
        connected = false;
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(connected) {
                    Router.getInstance().route(generator.getInstruction());
                }
            }
        }).start();
    }

    private void sendInstruction() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(connected) {
                    Router.getInstance().route(generator.getInstruction());
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    @Override
    public void onSteeringChanged(float value, int id) {
        if(useGyro)
            return;
        //Log.d("STEERING JOYSTICK", "value: " + value);
        generator.determineSteering((int)value);
        if(Math.abs(value) == Instruction.MAX_RATE) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (v != null) {
                v.vibrate(30);
            }
        }
    }

    @Override
    public void onAccelerationChanged(float value, int id) {
        if(useGyro)
            return;
        //Log.d("ACCELERATION JOYSTICK", "value: " + value);
        generator.determineDirection((int)value);
        if(Math.abs(value) == Instruction.MAX_RATE) {
            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            if (v != null) {
                v.vibrate(30);
            }
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(!useGyro)
            return;

        float[] rotationMatrix = new float[16];
        SensorManager.getRotationMatrixFromVector(
                rotationMatrix, event.values);

        // Convert to orientations
        float[] orientations = new float[3];
        SensorManager.getOrientation(rotationMatrix, orientations);

        for(int i = 0; i < 3; i++) {
            orientations[i] = (float)(Math.toDegrees(orientations[i]));
        }
//
//        Log.d("STEER: ", "val " + orientations[1]);
//        Log.d("ACCEL:",  "val " + orientations[2]);

        generator.gyroSteer((int)orientations[1]);
        generator.gyroAccel((int)orientations[2]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
