package com.simic.arduinocar.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

import com.simic.arduinocar.R;


/**
 *
 * Created by Nemanja Simic on 28 Jan 2018.
 */

public class SteeringJoystickView extends SurfaceView implements SurfaceHolder.Callback, OnTouchListener {

    private float width;

    private float height;

    private float baseX;

    private float baseY;

    private float batX;

    private float batY;

    private float batRadius;

    private SteeringJoystickListener joystickCallback;

    public SteeringJoystickView(Context context) {
        super(context);
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if(context instanceof SteeringJoystickListener )
            joystickCallback = (SteeringJoystickListener ) context;
    }

    public SteeringJoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if(context instanceof SteeringJoystickListener )
            joystickCallback = (SteeringJoystickListener ) context;
    }

    public SteeringJoystickView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if(context instanceof SteeringJoystickListener )
            joystickCallback = (SteeringJoystickListener ) context;
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        setDimensions();
        drawJoystick(batX, batY);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    public boolean onTouch(View v, MotionEvent e) {
        if(v.equals(this)) {
            if(e.getAction() != e.ACTION_UP) {
                float maxX =  batX + width / 2;
                float maxY = baseY + batRadius;
                float minX = batX - width / 2;
                float minY = baseY - batRadius;

                if((e.getX() < maxX && e.getY() < maxY)
                        && (e.getX() > minX && e.getY() > minY)) {

                    drawJoystick(e.getX(), batY);
                    if(e.getX() < baseX)
                        joystickCallback.onSteeringChanged( ((baseX - e.getX()) / (baseX - minX)) * (-30) - 100, getId());
                    else
                        joystickCallback.onSteeringChanged( ((e.getX() - baseX) / (maxX - baseX)) * 30 + 100, getId());
                } else {
                    if(e.getX() <= minX) {
                        drawJoystick(minX, batY);
                        joystickCallback.onSteeringChanged(-130, getId());
                    } else if(e.getX() >= maxX) {
                        drawJoystick(maxX, batY);
                        joystickCallback.onSteeringChanged(130, getId());
                    } else {
                        drawJoystick(batX, batY);
                        joystickCallback.onSteeringChanged(100, getId());
                    }
                }
            }
            else {
                drawJoystick(batX, batY);
                joystickCallback.onSteeringChanged(100, getId());
            }
        }

        return true;
    }

    private void drawJoystick(float newX, float newY) {
        if(getHolder().getSurface().isValid()) {
            Canvas canvas = this.getHolder().lockCanvas();
            Paint colors = new Paint();
            canvas.drawColor(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC);
            colors.setColor(getResources().getColor(R.color.colorPrimaryLight));
            canvas.drawRect(baseX - width / 2, baseY - height / 2,
                    baseX + width /2, baseY + height / 2, colors);
            colors.setColor(getResources().getColor(R.color.colorAccent));
            canvas.drawCircle(newX, newY, batRadius, colors);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void setDimensions() {
        width = getWidth() * 7 / 9;
        height = getHeight() / 3;
        baseY = getHeight() / 2;
        baseX = getWidth() / 2;
        batY = getHeight() / 2;
        batX = getWidth() / 2;
        batRadius = getHeight() / 3;
    }

    public interface SteeringJoystickListener {
        void onSteeringChanged(float value, int id);
    }
}
