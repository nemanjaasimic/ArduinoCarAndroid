package com.simic.arduinocar.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

import com.simic.arduinocar.R;

/**
 *
 * Created by Nemanja Simic on 28 Jan 2018.
 */

public class AccelerationJoystickView extends SurfaceView implements SurfaceHolder.Callback, OnTouchListener {

    private float width;

    private float height;

    private float baseX;

    private float baseY;

    private float batX;

    private float batY;

    private float batRadius;

    private AccelerationJoystickListener joystickCallback;

    public AccelerationJoystickView(Context context) {
        super(context);
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if(context instanceof AccelerationJoystickListener)
            joystickCallback = (AccelerationJoystickListener) context;
    }

    public AccelerationJoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if(context instanceof AccelerationJoystickListener)
            joystickCallback = (AccelerationJoystickListener) context;
    }

    public AccelerationJoystickView(Context context, AttributeSet attrs, int style) {
        super(context, attrs, style);
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if(context instanceof AccelerationJoystickListener)
            joystickCallback = (AccelerationJoystickListener) context;
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        setDimensions();
        drawJoystick(batX, batY);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    public boolean onTouch(View v, MotionEvent e) {
        if(v.equals(this)) {
            if(e.getAction() != e.ACTION_UP) {
                float maxX =  batX + batRadius;
                float maxY = baseY + height / 2;
                float minX = batX - batRadius;
                float minY = baseY - height / 2;

                if((e.getX() < maxX && e.getY() < maxY)
                        && (e.getX() > minX && e.getY() > minY)) {

                    drawJoystick(batX, e.getY());
                    if(e.getY() < baseY)
                        joystickCallback.onAccelerationChanged( ((baseY - e.getY()) / (baseY - minY)) * 80 + 100, getId());
                    else
                        joystickCallback.onAccelerationChanged( ((e.getY() - baseY) / (maxY -baseY)) * (-80) - 100, getId());
                } else {
                    if(e.getY() <= minY) {
                        drawJoystick(batX, minY);
                        joystickCallback.onAccelerationChanged(180, getId());
                    } else if(e.getY() >= maxY) {
                        drawJoystick(batX, maxY);
                        joystickCallback.onAccelerationChanged(-180, getId());
                    } else {
                        drawJoystick(batX, batY);
                        joystickCallback.onAccelerationChanged(100, getId());
                    }
                }
            }
            else {
                drawJoystick(batX, batY);
                joystickCallback.onAccelerationChanged(100, getId());
            }
        }

        return true;
    }

    private void drawJoystick(float newX, float newY) {
        if(getHolder().getSurface().isValid()) {
            Canvas canvas = this.getHolder().lockCanvas();
            Paint colors = new Paint();
            canvas.drawColor(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC);
            colors.setColor(getResources().getColor(R.color.colorPrimaryLight));
            canvas.drawRect(baseX - width / 2, baseY - height / 2,
                    baseX + width / 2, baseY + height / 2, colors);
            colors.setColor(getResources().getColor(R.color.colorAccent));
            canvas.drawCircle(newX, newY, batRadius, colors);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void setDimensions() {
        height = getHeight() * 7 / 9;
        width = getWidth() / 3;
        baseX = getWidth() / 2 ;
        baseY = getHeight() / 2;
        batX = getWidth() / 2;
        batY = getHeight() / 2;
        batRadius = getWidth() / 3;
    }

    public interface AccelerationJoystickListener {
        void onAccelerationChanged(float value, int id);
    }
}
