package com.simic.arduinocar.model;

import android.util.Log;

/**
 *
 * Created by Nemanja Simic on 29 Jan 2018.
 */

public class Instruction {

    public static int MAX_RATE = 180;

    public static int MIN_RATE = 100;

    private int direction;

    private int accelerationRate;

    private int steeringRate;

    private int equipment;

    public Instruction() {
        direction = Direction.NONE;
        accelerationRate = 180;
        steeringRate = 160;
        equipment = Equipment.NONE;
    }

    public String getCode() {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        builder.append(intToBinary(direction, 4));
        builder.append("] [");
        builder.append(accelerationRate);
        builder.append("] [");
        builder.append(steeringRate);
        builder.append("] [");
        builder.append(intToBinary(equipment, 8));
        builder.append("]");

        return builder.toString();
    }

    private static String intToBinary (int n, int numOfBits) {
        String number = "";
        int p = n;
        for(int i = 0; i < numOfBits; ++i, p/=2) {
            switch (p % 2) {
                case 0:
                    number = "0" + number;
                    break;
                case 1:
                    number = "1" + number;
                    break;
            }
        }

        return number;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int getAccelerationRate() {
        return accelerationRate;
    }

    public void setAccelerationRate(int accelerationRate) {
        if(accelerationRate < MIN_RATE || accelerationRate > MAX_RATE)
            throw new IllegalArgumentException("Acceleration rate must be between [100-180]");
        else
            this.accelerationRate = accelerationRate;
    }

    public int getSteeringRate() {
        return steeringRate;
    }

    public void setSteeringRate(int steeringRate) {
        if(steeringRate < MIN_RATE || steeringRate > MAX_RATE)
            throw new IllegalArgumentException("Steering rate must be between [100-130]");
        else
            this.steeringRate = steeringRate;
    }

    public int getEquipment() {
        return equipment;
    }

    public void setEquipment(int equipment) {
        this.equipment = equipment;
    }
}
