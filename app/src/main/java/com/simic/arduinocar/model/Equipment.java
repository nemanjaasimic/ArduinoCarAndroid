package com.simic.arduinocar.model;

/**
 *
 * Created by Nemanja Simic on 29 Jan 2018.
 */

public final class Equipment {

    public static int NONE                  = 0;
    public static int LEFT_INDICATOR        = 1;
    public static int RIGHT_INDICATOR       = 2;
    public static int TURN_ON               = 4;
    public static int HEADLIGHTS            = 8;
    public static int BLINK                 = 16;
    public static int ALARM                 = 32;
    public static int HAZARD_LIGHTS         = 64;
    public static int HORN                  = 128;
    public static int ALL_ON                = 255;
    public static int LEFT_INDICATOR_OFF    = 254;
    public static int RIGHT_INDICATOR_OFF   = 253;
    public static int TURN_OFF              = 251;
    public static int HEADLIGHTS_OFF        = 247;
    public static int NOT_BLINK             = 239;
    public static int ALARM_OFF             = 223;
    public static int HAZARD_LIGHTS_OFF     = 191;
    public static int HORN_OFF              = 127;


}
