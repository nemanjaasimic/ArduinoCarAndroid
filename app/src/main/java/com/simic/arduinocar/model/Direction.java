package com.simic.arduinocar.model;

/**
 *
 * Created by Nemanja Simic on 29 Jan 2018.
 */

public final class Direction {

    public static int NONE          = 0;
    public static int FORWARD       = 1;
    public static int BACKWARD      = 2;
    public static int RIGHT         = 4;
    public static int LEFT          = 8;
    public static int NOT_FORWARD   = 14;
    public static int NOT_BACKWARD  = 13;
    public static int NOT_RIGHT     = 11;
    public static int NOT_LEFT      = 7;
}